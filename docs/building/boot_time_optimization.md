# Boot Time Optimization

This is a guide for optimising boot times for automotive needs. Certain
components may need to be booted more quickly than on a traditional Linux
distribution. In the automotive industry, cold-boots are common and frequent,
so there is an expectation to boot quickly, to have a driveable system. Some
examples of components that may need to be prioritized could be camera,
ethernet, CAN bus, etc.

# Bootloader

Many bootloaders are the minimal implementation required to get a system
booted, here is a list of tunings that could be altered for optimization:

- disable serial logging
- maximize CPU clock for performance
- consider using multiple cores and parallelization
- consider accelerators for decompression

# Kernel

Some kernel arguments can be used to optimize boot time by disabling features.
You can also remove some kernel parameters. Documentation for kernel parameters
is available in the Linux git repo, https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/admin-guide/kernel-parameters.txt
Two such examples are below:

- `quiet` disables most log messages
- `libahci.ignore_sss=1` ignore staggered spin-up and spin-up all drives at once

# Initrd/Initramfs

Both the kernel and initrd require decompression of data, reading of the data
and writing it into memory, along with other initialization steps, like
populating a filesystem in the case of an initrd.

You may achieve faster build times by decreasing the size of your initrd
removing unused files, to list the files in initrd you can use the `lsinitrd`
command.

Building an initrd is done via a tool called `dracut` which pulls various files
from the main root filesystem into a compressed initrd archive.

The instructions for how to build the initrd are contained in the
/usr/lib/dracut/modules.d/ directory and primarily in the module-setup.sh files.
When altered these will change how your initrd is composed on next rebuild of
initrd.

Kernel modules are pulled from /usr/lib/modules/, removing a module and
rebuilding an initrd can be sufficient to decrease the size of an initrd.

# Userspace (and systemd)

systemd is designed to run as many units as possible in parallel that satisfies
the dependencies of the defined unit. Directives such as Wants=, Requires=,
Before=, After=, determine when your unit will start. For units that have to
start particularly early in boot DefaultDependencies=no can disable some
dependencies that are enabled by default. Here is an example of an application
that uses these features of systemd to start displaying a camera within 2
seconds.

```
[Unit]
Description=Show Twincam Boot Screen
DefaultDependencies=no
ConditionVirtualization=!container
IgnoreOnIsolate=true

[Service]
ExecStart=/usr/bin/twincam -d -s -u
Type=simple
RemainAfterExit=yes
KillMode=mixed
SendSIGKILL=no
```


# Tools

Here is a list of tools that can be used to help analyze and optimize
performance.


## systemd-analyze

systemd-analyze is a tool that may be used to determine system boot-up
performance statistics and retrieve other state and tracing information from
the system and service manager. Some examples of it's usage are below.

Basic command just to get high level boot statistics:

```
# systemd-analyze
```

If a systemd unit's dependencies are satisfied it proceeds, critical-chain can
be used to see the various points in a chain of units.

```
# systemd-analyze critical-chain
```

Plot an svg graphic to file, viewable in an svg viewer (most modern web
browsers are capable svg viewers):

```
# systemd-analyze plot > sd-plot.svg
```


## systemd-bootchart

systemd-bootchart is a tool that can be run at boot-up to collect CPU, memory,
I/O, and per-process timing information.  systemd-bootchart can be run at
boot-up by adding **init=/usr/lib/systemd/systemd-bootchart** to the kernel
command line.

systemd-bootchart collects data for 20 seconds and then outputs an SVG file
to /run/log, which can be viewed in a browser.

**initcall_debug** can also be added to the kernel command line in order to
include timing information for kernel init threads.

Options can be set in the file **/etc/systemd/bootchart.conf**.
